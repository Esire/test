<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactResource;
use App\Model\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = auth()->user()->contacts;
//        $contacts = Contact::query()->paginate();
//        if (\request()->has('size')) $contacts->appends('size', \request('size'));
        $contactsResourceCollection = ContactResource::collection($contacts);

        return $contactsResourceCollection;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = $request->user()->contacts()->create($request->all());

        return new ContactResource($contact);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return new ContactResource($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        if ($this->notUser($contact)) {
            return response()->json(['error' => 'Unauthorized to update this resource'], 401);
        }
        $contact->update($request->all());
        return new ContactResource($contact);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        if ($this->notUser($contact)) {
            return response()->json(['error' => 'Unauthorized to delete this resource'], 401);
        }
        $contact->delete();
        return response()->json(null, 200);
    }

    private function notUser(Contact $contact)
    {
        return auth()->id() != $contact->user_id;
    }
}
