<?php

namespace App\Http\Controllers\Api;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @var Client
     */
    private $http;
    private $clientId;
    private $clientSecret;
    private $grantType = 'password';

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $oauth_client = DB::table('oauth_clients')->where('password_client', 1)->first();
        $this->clientId = $oauth_client->id;
        $this->clientSecret = $oauth_client->secret;
        $this->http = new Client();
    }

    private function token($username, $password)
    {
        $response = $this->http->post(url('oauth/token'), [
            'form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'grant_type' => $this->grantType,
                'username' => $username,
                'password' => $password
            ]
        ])->getBody();

        return json_decode($response, true);
    }

    /**
     * Register user.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        $data = $this->validate($request, [
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:6|string',
            'username' => 'required|unique:users',
            'name' => 'sometimes|string|max:255'
        ]);

        $data['password'] = bcrypt($request->post('password'));

        $user = User::query()->create($data);
        $tokenInfo = $this->token($request->input('email'), $request->input('password'));

        return response()->json(['user' => $user] + $tokenInfo);
    }

    /**
     * Login in user.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, ['email' => 'required', 'password' => 'required']);
        $response = $this->token($request->input('email'), $request->input('password'));

        return response()->json($response);
    }

    /**
     * Refresh token.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function refreshToken(Request $request)
    {
        $this->validate($request, ['refresh_token' => 'required']);
        $response = $this->http->post(url('oauth/token'), [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'refresh_token' => $request->input('refresh_token')
            ]
        ])->getBody();

        $response = json_decode($response, true);

        return response()->json($response);
    }

    /**
     * Logs out user, deactivating the token.
     *
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        if (auth()->check()) {
            $loggedOut = $request->user()->token()->revoke();
            if ($loggedOut) return response()->json(['message' => 'Logged out Successfully']);
            else return response()->json(['message' => 'Log out failed'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['message' => 'Already logged out'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
