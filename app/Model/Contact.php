<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'phone'];

//    protected static function boot()
//    {
//        parent::boot();
//        // applies where logged in user on all contact queries.
//        self::addGlobalScope(function ($builder){
//            $builder->where('user_id', auth()->user()->id);
//        });
//    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
